export default {
  items: [
    {
      name: 'Transactions',
      url: '/',
      icon: 'icon-speedometer',
    },
    {
      name: 'New Transaction',
      url: '/transaction',
      icon: 'icon-puzzle'
    }
  ]
}
